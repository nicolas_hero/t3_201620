package parqueadero;



public class Cola <T>
{
	//------------------------ATRIBUTOS--------------------------
	
	//Nodo que hace referencia al elemento trasero de la cola.
	private Nodo back;
	
	//Nodo que hace referencia al elemento frontal de la cola.
	private Nodo front;
		
	//Cantidad de elementos almacenados en la pila.
	int tama�o;
	
	private class Nodo
	
	{
		
		private T elemento;
		
		private Nodo siguiente;	
		
		private Nodo anterior;
		
		
		
		Nodo(T pElemento)
		{
			elemento = pElemento;
			siguiente = null;
			anterior = null;
		}
		
		public T darElemento()
		{
			return elemento;
		}
		
		public Nodo darSiguiente()
		{
			return siguiente;
		}		
		
		public Nodo darAnterior()
		{
			return anterior;
		}	
		
	}
	
	public Nodo darBack()
	{
		return back;
	}
	
	public Nodo darFront()
	{
		return front;
	}
	
	public int darTama�o()
	{
		return tama�o;
	}
	
	
	public void encolar(T elemento)
	{
		Nodo encolado = new Nodo(elemento);
		
		if (vac�o())
		{
			front = encolado;
			back = encolado;
		}
		else
		{
			front.siguiente = encolado;
			encolado.anterior = front;
			front = encolado;
		}
		
		tama�o ++;
	}
	
	public T desencolar()
	{
		T desencolado = back.darElemento();
		
		if(vac�o())
		{
			return null;
		}
		else
		{
			if (back.siguiente != null)
			{
				back = back.siguiente;
				
				back.anterior = null;
			}
			else
			{
				back = null;
				front = null;
			}
			
			tama�o --;
			
			return desencolado;	
		}	
	}
	
	public boolean vac�o()
	{
		boolean esVac�o = false;
		
		if(back == null && tama�o == 0 && front == null)
		{
			esVac�o = true;
		}
		
		return esVac�o;
	}
	
}
