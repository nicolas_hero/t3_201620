package parqueadero;

import java.util.Date;
import java.util.Iterator;
import java.util.Queue;
import java.util.Stack;


public class Central <C>
{
    /**
     * Cola de carros en espera para ser estacionados
     */
	
	private Cola<Carro> colaEnEspera;
	
	
	/**
	 * Pilas de carros parqueaderos 1, 2, 3 .... 8
	 */
	
	private Pila<Carro>[] pilasParqueaderos;
	  
	
    /**
     * Pila de carros parqueadero temporal:
     * Aca se estacionan los carros temporalmente cuando se quiere
     * sacar un carro de un parqueadero y no es posible sacarlo con un solo movimiento
     */
	
	private Pila<Carro> pilaTemporal;
   
    
    /**
     * Inicializa el parqueadero: Los parqueaderos (1,2... 8) el estacionamiento temporal y la cola de carros que esperan para ser estacionados.
     */
    public Central ()
    {
        colaEnEspera = new Cola<Carro>();
        pilasParqueaderos = new Pila<Carro>();
        pilaTemporal = new Pila<Carro>();
    }
    
    /**
     * Registra un cliente que quiere ingresar al parqueadero y el vehiculo ingresa a la cola de carros pendientes por parquear
     * @param pColor color del vehiculo
     * @param pMatricula matricula del vehiculo
     * @param pNombreConductor nombre de quien conduce el vehiculo
     */
    public void registrarCliente (String pColor, String pMatricula, String pNombreConductor)
    {
    	Carro carroEntrante = new Carro(pColor, pMatricula, pNombreConductor);
    	colaEnEspera.encolar(carroEntrante);    	
    }    
    
    /**
     * Parquea el siguiente carro en la cola de carros por parquear
     * @return matricula del vehiculo parqueado y ubicaci�n
     * @throws Exception
     */
    public String parquearCarroEnCola() throws Exception
    {
    	String respuesta;
    	Carro porParquear = colaEnEspera.desencolar();
    	if (porParquear == null)
    	{
    		throw new Exception("No hay carros en la cola de esperar para parquear.");
    	}
    	else
    	{
    		String matriculaParqueado = porParquear.darMatricula();
    		String parqueoCarro = parquearCarro(porParquear);
    		
    		respuesta = matriculaParqueado + " " + parqueoCarro;
    	}
    	
        return respuesta;
    } 
    /**
     * Saca del parqueadero el vehiculo de un cliente
     * @param matricula del carro que se quiere sacar
     * @return El monto de dinero que el cliente debe pagar
     * @throws Exception si no encuentra el carro
     */
    public double atenderClienteSale (String matricula) throws Exception
    {
    	
    	return 0.0;
    }
    
  /**
   * Busca un parqueadero con cupo dentro de los 8 existentes y parquea el carro
   * @param aParquear es el carro que se saca de la cola de carros que estan esperando para ser parqueados
   * @return El parqueadero en el que qued� el carro.
   * @throws Exception
   */
    public String parquearCarro(Carro aParquear) throws Exception
    {
    	
    	String respuesta;
    	String ubicaci�n = "";
    	try
    	{
	    	int i = 1;
    	}
    	catch (Exception e)
    	{
    		throw new Exception("No hay parqueaderos con espacio alguno.");
    	}
    	
    	respuesta = ubicaci�n;
    	
    	return respuesta;    	    
    }
    
    /**
     * Itera sobre los tres parqueaderos buscando uno con la placa ingresada
     * @param matricula del vehiculo que se quiere sacar
     * @return el carro buscado
     */
    public Carro sacarCarro (String matricula)
    {

    	return null;
    }
    	
    /**
     * Saca un carro del parqueadero 1 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP1 (String matricula)
    {
    	return null;
    }
    
    /**
     * Saca un carro del parqueadero 2 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP2 (String matricula)
    {
    	return null;
    }
    
    /**
     * Saca un carro del parqueadero 3 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP3 (String matricula)
    {
    	return null;
    }
    /**
     * Saca un carro del parqueadero 4 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP4 (String matricula)
    {
    	
    	return null;
    }
    /**
     * Saca un carro del parqueadero 5 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP5 (String matricula)
    {
    	    	return null;
    }
    /**
     * Saca un carro del parqueadero 6 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP6 (String matricula)
    {
    	
    	return null;
    }
    /**
     * Saca un carro del parqueadero 7 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP7 (String matricula)
    {
    	
    	return null;
    }
    /**
     * Saca un carro del parqueadero 8 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP8 (String matricula)
    {
    	
    	return null;
    }
    /**
     * Calcula el valor que debe ser cobrado al cliente en funci�n del tiempo que dur� un carro en el parqueadero
     * la tarifa es de $25 por minuto
     * @param car recibe como parametro el carro que sale del parqueadero
     * @return el valor que debe ser cobrado al cliente 
     */
    public double cobrarTarifa (Carro car)
    {
    	return 0.0;	
    }
}
