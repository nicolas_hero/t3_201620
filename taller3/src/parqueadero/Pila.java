package parqueadero;

public class Pila <T>
{
	//------------------------ATRIBUTOS--------------------------
	
	//Nodo que hace referencia al tope de la pila.
	private Nodo tope; 
	
	//Cantidad de elementos almacenados en la pila.
	private int tama�o;
	
	private class Nodo
	
	{
		
		private T elemento;
		
		private Nodo siguiente;		
		
		
		Nodo(T pElemento)
		{
			elemento = pElemento;
			siguiente = null;
		}
		
		public T darElemento()
		{
			return elemento;
		}
		
		public Nodo darSiguiente()
		{
			return siguiente;
		}		
		
	}
	
	//------------------------CONSTRUCTOR------------------------
	
	public Pila()
	{
		tope = null;
		tama�o = 0;
	}
	
	//------------------------M�TODOS----------------------------
	
	public Nodo darTope()
	{
		return tope;
	}
	
	public int darTama�o()
	{
		return tama�o;
	}
	
	public boolean vac�o()
	{
		boolean esVac�o = false;
		if(tama�o == 0 && tope == null)
		{
			esVac�o = true;
		}
		
		return esVac�o;				
	}
	
	public void push(T pElemento)
	{
		Nodo nuevoTope = new Nodo(pElemento);
		
		if (vac�o())
		{
			tope = nuevoTope;
		}
		else
		{
			nuevoTope.siguiente = tope;
			tope = nuevoTope;
		}	
		
		tama�o ++;
	}
	
	public T pop()
	{
		if (vac�o())
		{
			return null;
		}
		else
		{
			T elementoTope = tope.elemento;
			
			if(tope.siguiente != null)
			{
				tope = tope.siguiente;			
			}
			else
			{
				tope = null;
			}
			tama�o --;
			
			return elementoTope;
		}
		
					
	}
}
